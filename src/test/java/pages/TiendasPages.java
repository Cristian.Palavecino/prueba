package pages;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.junit.Assert.*;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import logic.UtilityClass;


public class TiendasPages {
	private WebDriver driver;
	
	
	@FindBy(xpath="//a[text()='Tiendas']")
	WebElement linkTiendas;
	
	 
	 @FindBy(xpath="//div[@id='suc_14']//child::button[@aria-checked='false']")
	 WebElement ubicacionTienda;
	 
	UtilityClass utility = new UtilityClass(driver);
	
	 
	public TiendasPages(WebDriver driver) {
		this.driver= driver;
		PageFactory.initElements(driver,this);
	}
	
	public void ingresoLinkTiendas() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS); //Thread.sleep(1000); //for load
	}
	
	
	public void ejecutar() throws InterruptedException {
		
		
		String url ="https://www.pcfactory.cl/";
		boolean validar = utility.prueba(url);
		System.out.println("es : "+ validar);
		System.out.println(validar);
		System.out.println(url);
		
		
		if(validar) {
			assertTrue("La url correcta",true);
			linkTiendas.click();
		}else {
			assertFalse("La url es incorrecta", false);
			driver.close();
		}

	}
	
	
	public void intento (String url) throws Throwable {
		System.out.println(" Esta es la url capturada antes del if "+url);
		String comparar ="https://www.pcfactory.cl/";
		File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(screenshotFile, new File("Antes "+ System.currentTimeMillis()+".png"));
		}catch(IOException e){
			e.printStackTrace();
		}
		url = driver.getCurrentUrl();
		System.out.println(driver.getCurrentUrl()+"Este es el url a comparar");
		assertEquals(comparar, url);
		if(true) {
			File screenshotFile1 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshotFile, new File("Despues "+ System.currentTimeMillis()+".png"));
			}catch(IOException e){
				e.printStackTrace();
			}
			System.out.println(driver.getCurrentUrl()+"Este es el url a comparar");
			System.out.println("La URL es la correcta");
			linkTiendas.click();
			Thread.sleep(1000);	
		}else {
			assertTrue(false);
			System.out.println("La url no es la correcta");
			driver.close();
		}
		
	}
	
	
	public void prueba(String url) throws Throwable {
		//driver.get(url);
		System.out.println(" Esta es la url capturada antes del if "+url);
		String comparar ="https://www.pcfactory.cl/";
		File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(screenshotFile, new File("Antes "+ System.currentTimeMillis()+".png"));
		}catch(IOException e){
			e.printStackTrace();
		}
		if (driver.getCurrentUrl().equals(url)) {
			assertEquals(comparar, url);
			System.out.println(driver.getCurrentUrl()+"Este es el url a comparar");
			System.out.println("La URL es la correcta");
			File screenshotFile1 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshotFile, new File("Despues "+ System.currentTimeMillis()+".png"));
			}catch(IOException e){
				e.printStackTrace();
			}
			linkTiendas.click();
			Thread.sleep(1000);
			//utility.testScreenshot();
			
			
		} else {
			assertTrue(false);
			System.out.println("La url no es la correcta");
			driver.close();
		}
		
	}
	
	
}
