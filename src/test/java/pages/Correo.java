package pages;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import logic.UtilityClass;

public class Correo {
	
	private WebDriver driver;
	
	@FindBy(xpath="//input[@id='rdoPrvt']")
	 WebElement privado;

	@FindBy(xpath="//input[@id='username']")
	 WebElement usuario;
	
	@FindBy(xpath="//input[@id='password']")
	 WebElement contra;
	
	@FindBy(xpath="//input[@value='Sign in']")
	 WebElement btnSign;
	
	@FindBy(xpath="//*[@id=\"newmsgc\"]")
	 WebElement nuevo;
	
	@FindBy(xpath="//div[@id='divTo']")
	 WebElement para;
	
	@FindBy(xpath="//input[@id='txtSubj']")
	 WebElement asunto;
	
	@FindBy(xpath="//*[@id=\"ifBdy\"]")
	 WebElement texto;
	
	@FindBy(xpath="//*[@id=\"send\"]")
	 WebElement enviar;
	
	@FindBy(xpath="//*[@id=\"newmsgcd\"]")
	 WebElement abajo;
	
	@FindBy(xpath="//img[@id='imgToolbarButtonDropdownIcon']")
	 WebElement desplegable;
	
	@FindBy(xpath="//a[@id='newmtng']")
	 WebElement reunion;
	
	@FindBy(xpath="//input[@id='txtLoc']")
	 WebElement ubicacion;
	
	@FindBy(xpath="//*[@id=\"divSDate\"]/div/div")
	 WebElement fechaI;
	
	@FindBy(xpath="//div[@id='divSTime']//input[@id='txtTime']")
	 WebElement hora;
	
	@FindBy(xpath="")
	 WebElement x;
	
	@FindBy(xpath="")
	 WebElement c;
	
	@FindBy(xpath="")
	 WebElement v;
	
	@FindBy(xpath="")
	 WebElement q;
	
	
	UtilityClass utility = new UtilityClass(driver);
	
	 
	public Correo(WebDriver driver) {
		this.driver= driver;
		PageFactory.initElements(driver,this);
	}
	
	public void login() {
		privado.click();
		usuario.sendKeys("cristian.palavecino");
		contra.sendKeys("tuBJuP83");
		btnSign.click();
		
	}
	
	public void correo() throws Exception {
		nuevo.click();
		Thread.sleep(1000);
		
		String parentWindowHandler = driver.getWindowHandle(); // Almacena tu ventana actual
		String subWindowHandler = null;

		Set<String> handles = driver.getWindowHandles(); // Obten todas las ventana abiertas
		Iterator<String> iterator = handles.iterator();
		while (iterator.hasNext()){
		    subWindowHandler = iterator.next();
		}
		driver.switchTo().window(subWindowHandler);
		
		para.sendKeys("emilio.silva@colmena.cl");
		Thread.sleep(1000);
		asunto.sendKeys("Prueba correo automático");
		Thread.sleep(1000);
		texto.sendKeys("Muy buenas tarde esto es una prueba para enviar correos de forma automática");
		Thread.sleep(1000);
		enviar.click();
	}
	
	public void reunion() throws Exception {
		
		desplegable.click();
		reunion.click();
		Thread.sleep(1000);
		String parentWindowHandler = driver.getWindowHandle(); // Almacena tu ventana actual
		String subWindowHandler = null;

		Set<String> handles = driver.getWindowHandles(); // Obten todas las ventana abiertas
		Iterator<String> iterator = handles.iterator();
		while (iterator.hasNext()){
		    subWindowHandler = iterator.next();
		}
		driver.switchTo().window(subWindowHandler);
		
		para.sendKeys("emilio.silva@colmena.cl");
		Thread.sleep(1000);
		
		asunto.sendKeys("Prueba reunion automático");
		Thread.sleep(1000);
		ubicacion.sendKeys("Google meet");
		Thread.sleep(1000);
		//fechaI.click();
		hora.sendKeys(Keys.CONTROL +"a");
		hora.sendKeys(Keys.DELETE);
		Thread.sleep(1000);
		hora.sendKeys("19:00");
		Thread.sleep(1000);
		texto.sendKeys("Probando automatización de reunión");
		//enviar.click();
	}
	
	
	
	
}
