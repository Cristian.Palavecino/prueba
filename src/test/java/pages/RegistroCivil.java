package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import logic.UtilityClass;

public class RegistroCivil {
	private WebDriver driver;
	
	@FindBy(xpath="//*[@id=\"FormSearch\"]/div/div[1]/input")
	 WebElement buscarTramite;
	
	@FindBy(xpath="//*[@id=\"btn_search\"]/strong")
	 WebElement btnBuscar;
	
	@FindBy(xpath="//*[@id=\"search\"]/div[1]/div[3]/div[2]/ol/li[5]/div[1]/div[1]")
	 WebElement paraTodo;
	
	@FindBy(xpath="/html[1]/body[1]/div[1]/main[1]/div[1]/div[1]/div[2]/div[2]/div[2]/div[2]/div[1]/div[1]/div[1]/button[1]")
	 WebElement btnObtener;
	
	@FindBy(xpath="//*[@id=\"redirect-modal\"]/div/div/div[3]/a")
	 WebElement irTramite;
	
	
	
	UtilityClass utility = new UtilityClass(driver);
	
	 
	public RegistroCivil(WebDriver driver) {
		this.driver= driver;
		PageFactory.initElements(driver,this);
	}
	
	public void buscar() throws InterruptedException {
		buscarTramite.sendKeys("Certificado de nacimiento");
		btnBuscar.click();
		Thread.sleep(1000);	
		paraTodo.click();
		Thread.sleep(1000);	
		btnObtener.click();
		Thread.sleep(1000);	
		irTramite.click();
		Thread.sleep(1000);	
		
		
	}
	
	
}
