package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import logic.UtilityClass;

public class Certificado {
	
	private WebDriver driver;
	
	@FindBy(xpath="//div[@class='icheckbox_minimal-grey hover']//ins[@class='iCheck-helper']")
	 WebElement check;
	
	@FindBy(xpath="//input[@id='cu_inputRUN']")
	 WebElement rut;
	
	@FindBy(xpath="//input[@id='cu_inputClaveUnica']")
	 WebElement contraseņa;
	
	@FindBy(xpath="//button[@id='cu_btnIngresar']")
	 WebElement autenticar;
	
	@FindBy(xpath="//button[@id='carro_btnContinuar']")
	 WebElement continuar;
	
	@FindBy(xpath="//div[@id='title_0']")
	 WebElement desplegable;
	
	@FindBy(xpath="")
	 WebElement b;
	
	@FindBy(xpath="")
	 WebElement c;
	
	@FindBy(xpath="")
	 WebElement d;
	
	UtilityClass utility = new UtilityClass(driver);
	
	 
	public Certificado(WebDriver driver) {
		this.driver= driver;
		PageFactory.initElements(driver,this);
	}
	
	public void login() throws InterruptedException {
		Thread.sleep(1000);
		desplegable.click();
		Thread.sleep(1000);
		desplegable.click();
		Thread.sleep(1000);
		check.click();
		rut.sendKeys("20318658-4");
		contraseņa.sendKeys("Graustert06.");
		autenticar.click();
		Thread.sleep(1000);
		continuar.click();
	}
}
