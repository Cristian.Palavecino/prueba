package pruebas;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import logic.UtilityClass;
import pages.Certificado;
import pages.Correo;
import pages.RegistroCivil;
import pages.TiendasPages;

public class TestPrueba {
	private WebDriver driver;
	UtilityClass util;
	TiendasPages tienda;
	RegistroCivil registro;
	Certificado certificado;
	Correo correo;
	
	
	@Before
	public void configuracion() {
		util= new UtilityClass(driver);
		driver= util.iniciarChrome();
		tienda= new TiendasPages(driver);
		registro = new RegistroCivil(driver);
		certificado = new Certificado(driver);
		correo = new Correo(driver);
	}
	
	@Test
	public void test() throws Throwable{
	
		correo.login();
		//correo.correo();
		correo.reunion();
	}
}
