package logic;


import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.hamcrest.Description;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class UtilityClass {
	private WebDriver driver;
	String url="https://webmail.colmena.cl/owa/auth/logon.aspx?replaceCurrent=1&reason=3&url=https%3a%2f%2fwebmail.colmena.cl%2fowa%2f";
	
	public UtilityClass(WebDriver driver) {
		this.driver= driver;
	}
	
	
	public WebDriver iniciarChrome() {
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver/chromedriver.exe");
		driver= new ChromeDriver();
		driver.get(url);
		driver.manage().window().maximize();
		return driver;
	}
	
	public boolean prueba(String url) throws InterruptedException {
		boolean confirmar;
		//driver.get(url);
		System.out.println(" Esta es la url capturada antes del if: "+url);
		System.out.println(" Esta es la url capturada antes del if: "+ driver.getCurrentUrl() );
		if (driver.getCurrentUrl().equals(url)) {
			System.out.println(driver.getCurrentUrl()+"Este es el url a comparar");
			System.out.println("Las url son iguales");		
			confirmar = true;
		} else {
			System.out.println("Las url no son iguales");	
			confirmar = false;
		}
		System.out.println("es : " + confirmar);	
		 return confirmar;
	}
	
	public void testScreenshot() {
		
		File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(screenshotFile, new File("Imagen "+ System.currentTimeMillis()+".png"));
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}
	
	
	
	
}
